PFExtractImages - Extract images from maps like in CW3!

Usage:
When editting a map, press `i`. This will extract images from the map files to the `editor/<mapname>/extractedimages` directory, just like in CW3!

Installation instructions:

- Download [Release.zip from the repo](https://gitlab.com/KubeRoot/PFExtractImages/raw/master/Release.zip)
- Unpack into Particle Fleet Emergence\ParticleFleet_Data\Managed
- Run patch.bat

NOTE:
If you want to use this mod with PFBetterScreenshot, I recommend using the patch.bat file included with this mod. Mods should be compatible with each other and the patch file should install all mods currently in the directory at once, but the one included with PFExtractImages is better coded.