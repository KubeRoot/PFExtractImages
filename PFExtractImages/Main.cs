﻿using MonoMod;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class patch_InputManager : InputManager
{

    [MonoModIgnore]
    public patch_InputManager(GameControls gameControls, Camera mainCamera) : base(gameControls, mainCamera) { }

    public extern void orig_HandleInput();
    public void HandleInput()
    {
        orig_HandleInput();
        if (GameSpace.editMode)
        {
            CheckImageExtractionKeyPress();
        }
    }

    private void CheckImageExtractionKeyPress()
    {
        if (Input.GetKeyDown("i"))
        {
            List<Texture2D> images = GameSpace.instance.customImages.GetUnitImages();
            string dir = FileManager.GetEditorBaseDir() + GameSpace.editorDirName + "/extractedimages/";
            try
            {
                Directory.CreateDirectory(dir);
            }
            catch(Exception e) { return; }
            for (int i = 0; i < images.Count; i++)
            {
                try
                {
                    if (images[i] != null)
                    {
                        byte[] imageData = images[i].EncodeToPNG();
                        string p = dir + "image" + i.ToString() + ".png";
                        File.WriteAllBytes(p, imageData);
                    }
                }
                catch (Exception e) { }
            }
        }
    }
}